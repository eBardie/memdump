#include <fcntl.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/time.h>
#include <unistd.h>

#include "memdump.h"
#include "memdump_colours.h"

// -----------------------------------------------------------------------------
//
// Override the default configured colourscheme, if any.

#ifdef MEMDUMP_TEST_COLOURSCHEME_OVERRIDE
memdump_colourscheme_t memdump_colourscheme = {
    {
        HGRNB, HYEL, FAINT HYEL, HYEL,
        HGRNB, HYEL, FAINT HYEL, HYEL,
        HGRNB, HYEL, FAINT HYEL, HYEL,
        HGRNB, HYEL, FAINT HYEL, HYEL,
    },{
        GRNB, YEL, FAINT YEL, YEL,
        GRNB, YEL, FAINT YEL, YEL,
        GRNB, YEL, FAINT YEL, YEL,
        GRNB, YEL, FAINT YEL, YEL,
    }
};
#endif

// -----------------------------------------------------------------------------
//
// Load the first (up to) buflen chars of a file in to a buffer, and dump the
// buffer.

void dumpfile(char *filepath) {

    int fd = open(filepath, O_RDONLY);

    if (fd >= 0) {
        int buflen = 256;
        char buf[buflen];
        ssize_t r = read(fd, buf, buflen);
        if (r >= 0) {
            memdump(filepath, buf, r);
        } else {
            fprintf(stderr, "Can't read from '%s' : %m\n", filepath);
        }
    } else {
        fprintf(stderr, "Can't open '%s' : %m\n", filepath);
    }
}

// -----------------------------------------------------------------------------

int main(int argc, char* argv[]) {
    // Setting up locale is only important if attempting to display non-ASCII chars.
    if (!setlocale(LC_ALL, ""))
       fprintf(stderr, "Warning: Current locale is not supported by the C library.\n");


    int buflen = 128;

    // Dump the given buffer, buf. Do this multiple times, display N bytes of
    // the buffer, where N goes from 1 to buflen
    unsigned char buf[] = "0123456789abcdef0123456789ABCDEFfedcba9876543210FEDCBA98765432100123456789abcdef0123456789ABCDEFfedcba9876543210FEDCBA9876543210";

    for (int i = 0; i <= buflen; ++i) {
        memdump("\nbuf", buf, i);
    }

    // Create a buffer with random values
    unsigned char rand_buf[buflen];

    struct timeval tv;
    gettimeofday(&tv, NULL);
    srand(tv.tv_usec);

    for (int i = 0; i < buflen; ++i) {
        rand_buf[i] = (char)(rand() % 256);
    }

    // Dump the random buffer
    memdump("\nrand_buf", rand_buf, buflen);

    // Dump the binary executable of this file, and any files passed on the command line
    for (int i = 0; i < argc; ++i) {
        dumpfile(argv[i]);
    }

    return 0;
}
