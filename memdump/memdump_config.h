#ifndef _MEMDUMP_CONFIG_H
#define _MEMDUMP_CONFIG_H

#include "memdump.h"

// -----------------------------------------------------------------------------
//
// Options for use in the configuration section below, which has more details

#define HEX_NO_SEPARATOR   0
#define HEX_UNIT_SEPARATOR 1
#define HEX_PAIR_SEPARATOR 2

#define GUTTER_NONE 0
#define GUTTER_HALF 1
#define GUTTER_HALVE 1
#define GUTTER_QUARTER 2

#define CS_NONE                      0
#define CS_GREEN_RED                 1
#define CS_GREEN_RED_BG              2
#define CS_GREEN_RED_BOLD            3
#define CS_GREEN_RED_OFFSET          4
#define CS_GREEN_RED_OFFSET_BG       5
#define CS_GREEN_RED_OFFSET_BOLD     6
#define CS_MULTICOLOUR               7
#define CS_MULTICOLOUR_BG            8
#define CS_UNDERLINE                 9
#define CS_GREEN_BOLD               10
#define CS_BOLD                     11
#define CS_BLUE                     12
#define CS_BLUE_BG                  13
#define CS_BLUE_BOLD                14
#define CS_BLUE_ON_BLUE_BG          15

// -----------------------------------------------------------------------------
//
// If you want to emulate `xxd`, set the following to 1, otherwise set to 0 and
// adjust the individual USE_ prefix parameters to taste in the `else` section
// below.

#define EMULATE_XXD 0

#if EMULATE_XXD

// Don't adjust these XXD emulation values
#define USE_GUTTER GUTTER_NONE
#define USE_DISPLAY_ALL_PRINTABLE 0
#define USE_COLOUR CS_NONE
#define USE_HEX_SEPARATOR HEX_PAIR_SEPARATOR

#else // If EMULATE_XXD is set to 0, adjust the USE_ prefixed definitions in this section

// To add legiblity gutter(s), set USE_GUTTER to one of the non-NONE GUTTER_
// prefixed values above.  Otherwise set it to GUTTER_NONE.
//
// Note that this breaks strict xxd compatibility, but may aid legibility.
//
// c.f. find the hex value of the first `.` after the `]` sign on the third row.:w
//
// GUTTER_NONE:
//
// 0x7ffca2633a00: 67c6 6973 51ff 4aec 29cd baab f2fb e346  g.isQ.J.)......F
// 0x7ffca2633a10: 7cc2 54f8 1be8 e78d 765a 2e63 339f c99a  |.T.....vZ.c3...
// 0x7ffca2633a20: 6632 0db7 3158 a35a 255d 0517 58e9 5ed4  f2..1X.Z%]..X.^.
// 0x7ffca2633a30: abb2 cdc6 9bb4 5411 0e82 7441 213d dc    ......T...tA!=.
//
// GUTTER_HALF:
//
// 0x7ffca2633a00: 67c6 6973 51ff 4aec  29cd baab f2fb e346  g.isQ.J. )......F
// 0x7ffca2633a10: 7cc2 54f8 1be8 e78d  765a 2e63 339f c99a  |.T..... vZ.c3...
// 0x7ffca2633a20: 6632 0db7 3158 a35a  255d 0517 58e9 5ed4  f2..1X.Z %]..X.^.
// 0x7ffca2633a30: abb2 cdc6 9bb4 5411  0e82 7441 213d dc    ......T. ..tA!=.

// GUTTER_QUARTER:
//
// 0x7ffca2633a00: 67c6 6973  51ff 4aec  29cd baab  f2fb e346  g.is Q.J. )... ...F
// 0x7ffca2633a10: 7cc2 54f8  1be8 e78d  765a 2e63  339f c99a  |.T. .... vZ.c 3...
// 0x7ffca2633a20: 6632 0db7  3158 a35a  255d 0517  58e9 5ed4  f2.. 1X.Z %].. X.^.
// 0x7ffca2633a30: abb2 cdc6  9bb4 5411  0e82 7441  213d dc    .... ..T. ..tA !=.

#define USE_GUTTER GUTTER_QUARTER


// When active gutters always appear in the hex representation.
// If you want gutters in the char representation too, set USE_CHAR_GUTTER to 1,
// otherwise set it to 0
//
// c.f USE_CHAR_GUTTER 1
//
// 0x7fff6b0bab60: 2f 2f 20 53  50 44 58 2d  4c 69 63 65  6e 73 65 2d  //.S PDX- Lice nse-
// 0x7fff6b0bab70: 49 64 65 6e  74 69 66 69  65 72 3a 20  47 50 4c 2d  Iden tifi er:. GPL-
// 0x7fff6b0bab80: 32 2e 30 2b  0a 2f 2a 0a  20 2a 20 43  6f 70 79 72  2.0+ ./*. .*.C opyr
//
// vs. USE_CHAR_GUTTER 0
//
// 0x7ffdf7303120: 2f 2f 20 53  50 44 58 2d  4c 69 63 65  6e 73 65 2d  //.SPDX-License-
// 0x7ffdf7303130: 49 64 65 6e  74 69 66 69  65 72 3a 20  47 50 4c 2d  Identifier:.GPL-
// 0x7ffdf7303140: 32 2e 30 2b  0a 2f 2a 0a  20 2a 20 43  6f 70 79 72  2.0+./*..*.Copyr

#define USE_CHAR_GUTTER 0


// Separate out the hex representation in blocks.  A unit is the two hex chars,
// a pair is two units, so four chars in total.  Some colourschemes work better
// with one or the other.  Set to HEX_NO_SEPARATOR for no separation
//
// HEX_PAIR_SEPARATOR:
//   0x10d0: 9e36 9a1f 05fe 29a2 b7b3 3ee6 133f c1a2  .6....)...>..?..
//
// HEX_UNIT_SEPARATOR:
//   0x10d0: 9e 36 9a 1f 05 fe 29 a2 b7 b3 3e e6 13 3f c1 a2  .6....)...>..?..
//
// HEX_NO_SEPARATOR:
//   0x10d0: 9e369a1f05fe29a2 b7b33ee6133fc1a2  .6....)...>..?..

#define USE_HEX_SEPARATOR HEX_UNIT_SEPARATOR


// xxd doesn't emit printable non-ASCII chars. Set USE_DISPLAY_ALL_PRINTABLE to 1
// if you want to display all printable chars, otherwise set to 0 for ASCII only.
// Non-ASCII behaviour is locale dependent, and different terminals will behave
// differently.  This means that to emit non-ASCII printable chars, the
// `setlocale` function must have been called with a system appropriate value,
// maybe simply an empty string ("") if the environment is already configured.
// See `man setlocale`.

#define USE_DISPLAY_ALL_PRINTABLE 0


// Use a colourscheme to visually correlate bytes between their hex and char
// representations.
//
// Note that "colour" also covers bold, faint, and underlined.  How, or even
// if, these are implemented varies from terminal to terminal.  If you're
// dumping to a file, you'll also get the escape codes when using a
// colourscheme.
//
// To use a colourscheme, set USE_COLOUR to one of CS_ prefixed
// colourscheme IDs.
//
// To override with a custom colourscheme, set USE_COLOUR to any CS_ value other
// than CS_NONE.  See comments in the memdump.h file for further information.
//
// To not use any colourscheme, set USE_COLOUR to CS_NONE.

#define USE_COLOUR CS_BOLD

#endif // EMULATE_XXD    No more configuration options below this line.

// -----------------------------------------------------------------------------

#if USE_DISPLAY_ALL_PRINTABLE
#define ISASCII(c) 1
#else
#define ISASCII(c) isascii(c)
#endif

#define WIDTH 16
#define THREE_QUARTER_WIDTH 12
#define HALF_WIDTH 8
#define QUARTER_WIDTH 4

// -----------------------------------------------------------------------------

#if USE_COLOUR

#include "memdump_colours.h"

memdump_colourscheme_t memdump_colourscheme __attribute__ ((weak)) = {
#if USE_COLOUR == CS_GREEN_RED
    {
        GRN, GRN, RESET, RESET,
        GRN, GRN, RESET, RESET,
        GRN, GRN, RESET, RESET,
        GRN, GRN, RESET, RESET,
    },{
        RED, RED, RESET, RESET,
        RED, RED, RESET, RESET,
        RED, RED, RESET, RESET,
        RED, RED, RESET, RESET,
    }
#elif USE_COLOUR == CS_GREEN_RED_BG
    {
        GRNBG, GRNBG, RESET, RESET,
        GRNBG, GRNBG, RESET, RESET,
        GRNBG, GRNBG, RESET, RESET,
        GRNBG, GRNBG, RESET, RESET,
    },{
        REDBG, REDBG, RESET, RESET,
        REDBG, REDBG, RESET, RESET,
        REDBG, REDBG, RESET, RESET,
        REDBG, REDBG, RESET, RESET,
    }
#elif USE_COLOUR == CS_GREEN_RED_BOLD
    {
        GRNB, GRNB, RESET, FAINT,
        GRNB, GRNB, RESET, FAINT,
        GRNB, GRNB, RESET, FAINT,
        GRNB, GRNB, RESET, FAINT,
    },{
        REDB, REDB, RESET, FAINT,
        REDB, REDB, RESET, FAINT,
        REDB, REDB, RESET, FAINT,
        REDB, REDB, RESET, FAINT,
    }
#elif USE_COLOUR == CS_GREEN_RED_OFFSET
    {
        GRN, GRN, RESET, RESET,
        GRN, GRN, RESET, RESET,
        GRN, GRN, RESET, RESET,
        GRN, GRN, RESET, RESET,
    },{
        RESET, RESET, RED, RED,
        RESET, RESET, RED, RED,
        RESET, RESET, RED, RED,
        RESET, RESET, RED, RED,
    }
#elif USE_COLOUR == CS_GREEN_RED_OFFSET_BG
    {
        GRNBG, GRNBG, RESET, RESET,
        GRNBG, GRNBG, RESET, RESET,
        GRNBG, GRNBG, RESET, RESET,
        GRNBG, GRNBG, RESET, RESET,
    },{
        RESET, RESET, REDBG, REDBG,
        RESET, RESET, REDBG, REDBG,
        RESET, RESET, REDBG, REDBG,
        RESET, RESET, REDBG, REDBG,
    }
#elif USE_COLOUR == CS_GREEN_RED_OFFSET_BOLD
    {
        GRNB, GRNB, RESET, FAINT,
        GRNB, GRNB, RESET, FAINT,
        GRNB, GRNB, RESET, FAINT,
        GRNB, GRNB, RESET, FAINT,
    },{
        FAINT, RESET, REDB, REDB,
        FAINT, RESET, REDB, REDB,
        FAINT, RESET, REDB, REDB,
        FAINT, RESET, REDB, REDB,
    }
#elif USE_COLOUR == CS_MULTICOLOUR
    {
        RED, BLU, RED, BLU,
        RED, BLU, RED, BLU,
        RED, BLU, RED, BLU,
        RED, BLU, RED, BLU,
    }, {
        GRN, YEL, GRN, YEL,
        GRN, YEL, GRN, YEL,
        GRN, YEL, GRN, YEL,
        GRN, YEL, GRN, YEL,
    }
#elif  USE_COLOUR == CS_MULTICOLOUR_BG
    {
        REDBG, BLUBG, REDBG, BLUBG,
        REDBG, BLUBG, REDBG, BLUBG,
        REDBG, BLUBG, REDBG, BLUBG,
        REDBG, BLUBG, REDBG, BLUBG,
    }, {
        GRNBG, YELBG, GRNBG, YELBG,
        GRNBG, YELBG, GRNBG, YELBG,
        GRNBG, YELBG, GRNBG, YELBG,
        GRNBG, YELBG, GRNBG, YELBG,
    }
#elif USE_COLOUR == CS_UNDERLINE
    {
        UND FAINT, RESET, RESET, RESET,
        UND FAINT, RESET, RESET, RESET,
        UND FAINT, RESET, RESET, RESET,
        UND FAINT, RESET, RESET, RESET,
    },{
        UND FAINT, RESET, RESET, RESET,
        UND FAINT, RESET, RESET, RESET,
        UND FAINT, RESET, RESET, RESET,
        UND FAINT, RESET, RESET, RESET,
    }
#elif USE_COLOUR == CS_BOLD
    {
        BOL, RESET FAINT, RESET, RESET FAINT,
        BOL, RESET FAINT, RESET, RESET FAINT,
        BOL, RESET FAINT, RESET, RESET FAINT,
        BOL, RESET FAINT, RESET, RESET FAINT,
    },{
        BOL, RESET FAINT, RESET, RESET FAINT,
        BOL, RESET FAINT, RESET, RESET FAINT,
        BOL, RESET FAINT, RESET, RESET FAINT,
        BOL, RESET FAINT, RESET, RESET FAINT,
    }
#elif USE_COLOUR == CS_BLUE
    {
        BLU, RESET, RESET, RESET,
        BLU, RESET, RESET, RESET,
        BLU, RESET, RESET, RESET,
        BLU, RESET, RESET, RESET,
    },{
        BLU, RESET, RESET, RESET,
        BLU, RESET, RESET, RESET,
        BLU, RESET, RESET, RESET,
        BLU, RESET, RESET, RESET,
    }
#elif USE_COLOUR == CS_BLUE_BG
    {
        HBLUBG, RESET, RESET, RESET,
        HBLUBG, RESET, RESET, RESET,
        HBLUBG, RESET, RESET, RESET,
        HBLUBG, RESET, RESET, RESET,
    },{
        HBLUBG, RESET, RESET, RESET,
        HBLUBG, RESET, RESET, RESET,
        HBLUBG, RESET, RESET, RESET,
        HBLUBG, RESET, RESET, RESET,
    }
#elif USE_COLOUR == CS_BLUE_BOLD
    {
        HBLUB, FAINT, RESET, FAINT,
        HBLUB, FAINT, RESET, FAINT,
        HBLUB, FAINT, RESET, FAINT,
        HBLUB, FAINT, RESET, FAINT,
    },{
        HBLUB, FAINT, RESET, FAINT,
        HBLUB, FAINT, RESET, FAINT,
        HBLUB, FAINT, RESET, FAINT,
        HBLUB, FAINT, RESET, FAINT,
    }
#elif USE_COLOUR == CS_GREEN_BOLD
    {
        HGRNB, FAINT, RESET, FAINT,
        HGRNB, FAINT, RESET, FAINT,
        HGRNB, FAINT, RESET, FAINT,
        HGRNB, FAINT, RESET, FAINT,
    },{
        HGRNB, FAINT, RESET, FAINT,
        HGRNB, FAINT, RESET, FAINT,
        HGRNB, FAINT, RESET, FAINT,
        HGRNB, FAINT, RESET, FAINT,
    }
#else // USE_COLOUR == CS_BLUE_ON_BLUE
    {
        BLUBG, BLUBG, HBLUBG, HBLUBG,
        BLUBG, BLUBG, HBLUBG, HBLUBG,
        BLUBG, BLUBG, HBLUBG, HBLUBG,
        BLUBG, BLUBG, HBLUBG, HBLUBG,
    },{
        HBLUBG, HBLUBG, BLUBG, BLUBG,
        HBLUBG, HBLUBG, BLUBG, BLUBG,
        HBLUBG, HBLUBG, BLUBG, BLUBG,
        HBLUBG, HBLUBG, BLUBG, BLUBG,
    }
#endif
};

#define LINE_FMT FAINT "%p:" UNFAINT " %s\n"
#define COLOUR(row, col) memdump_colourscheme[row][col]
#else // !USE_COLOUR
#define RESET ""
#define LINE_FMT "%p: %s\n"
#endif

#endif // _MEMDUMP_CONFIG_H
