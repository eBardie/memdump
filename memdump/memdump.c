#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include "memdump.h"
#include "memdump_config.h"

// -----------------------------------------------------------------------------

static inline void _memdump_append_char_display(int odd_row, char* buf, unsigned char *src, unsigned int count) {
    // Always a two space gutter between hex and char displays
#if USE_COLOUR
    strcat(buf, RESET "  ");
#else
    strcat(buf, "  ");
#endif // USE_COLOUR

    // Loop over index of bytes to display
    for (unsigned int j = 0; j < count; ++j) {

#if USE_GUTTER && USE_CHAR_GUTTER
#if USE_GUTTER == GUTTER_QUARTER
        // 1/4, 1/2, and 3/4, but not 4/4
        if (j % QUARTER_WIDTH == 0 && ((j % WIDTH) != 0)) {
#else
        if (j == HALF_WIDTH) {
#endif // USE_GUTTER == GUTTER_QUARTER
#if USE_COLOUR
            strcat(buf, RESET " ");
        } else {
            strcat(buf, RESET);
        }
#else
            strcat(buf, " ");
        }
#endif // USE_COLOUR within USE_GUTTER
#else
        strcat(buf, RESET);
#endif // USE_GUTTER && USE_CHAR_GUTTER

#if USE_COLOUR
        strcat(buf, COLOUR(odd_row, j));
#endif // USE_COLOUR

        // Add a char for the byte to the buffer
        unsigned char c = src[j];
        int i = strlen(buf);
        buf[i] = (isgraph(c) && ISASCII(c)) ? c : L'.';
        buf[i+1] = '\0';
    }
#if USE_COLOUR
    strcat(buf, RESET);
#endif
}

// -----------------------------------------------------------------------------
//
// Display memory, more-or-less following xxd's output format.
//
// Parameters:
//
//    file : stdio FILE structure.  Usually `stdout` or `stderr`, but could be custom.
//    desc : string descrbing the (expected!) content of the block of memory.  This is
//           skipped if NULL.
//    addr :  address of the start of block of memory
//    lenin: length in bytes of the block of memory

void fmemdump(FILE *file, char *desc, void *addr, unsigned int lenin) {
    int odd_row = 0;

    // Buffer needs to contain:  2 * WIDTH for the hex representation of each byte
    //                           1 * WIDTH for the spaces between each pair of hex representations
    //                           1 * WIDTH for the char representation
    //                           2 for the always present gutter between hex and char displays
    //
    // Note that this initialization is spread over multiple lines of
    // conditional compilation below.  Play hunt the semi-colon.
    //
    // Note too that for most usable colourschemes this ends up with a lot of
    // spare space in the buffer.  Sizing according to the actual colourscheme
    // in use would be preferable, but not obvious how to do this at compile
    // time.
    unsigned int line_buf_size = (int)(WIDTH * 4) + 2
#if USE_GUTTER
    // Account for extra guttering
#if USE_GUTTER == GUTTER_QUARTER
    + 6
#else
    + 2
#endif // USE_GUTTER == GUTTER_QUARTER
#if USE_COLOUR
    // Gutter adds a RESET for colour mode
    + MAX_COLOUR_CODE_LEN
#if USE_HEX_SEPARATOR == HEX_UNIT_SEPARATOR
    // Unit separator mode adds a RESET for each unit
    + WIDTH * MAX_COLOUR_CODE_LEN
#elif USE_HEX_SEPARATOR == HEX_PAIR_SEPARATOR
    // Pair separator mode adds a RESET for each pair
    + HALF_WIDTH * MAX_COLOUR_CODE_LEN
#endif // USE_HEX_SEPARATOR
#endif // USE_COLOUR
#endif // USE_GUTTER
#if USE_COLOUR // Independent of USE_GUTTER
    // Account for colour codes and each byte's hex and char representations,
    // and a line reset.
    + (WIDTH * 2 + 1) * MAX_COLOUR_CODE_LEN
#endif
    ; // Close the initalization of unsigned int line_buf_size

    if (desc) {
        fprintf(file, "%s:\n", desc);
    }

    unsigned char *line_src = addr;
    unsigned int remaining = lenin;
    char hex_buf[3];
    char line_buf[line_buf_size];
    line_buf[0] = '\0';

    // Iterate over input
    for (void *p = addr; (p < (addr + lenin)); ++p) {
        // After the first byte
        if (p != addr) {
            // Finish off complete lines
            if ((p - addr) % WIDTH == 0) {
                _memdump_append_char_display(odd_row, line_buf, line_src, WIDTH);
                fprintf(file, LINE_FMT, line_src, line_buf);
                //fprintf(stdout, " buf size: %u  used: %lu\n", line_buf_size, strlen(line_buf) + 1);

                // Reset for next line
                remaining -= WIDTH;
                line_buf[0] = '\0';
                line_src = p;
                odd_row = odd_row == 0 ? 1 : 0;
#if USE_GUTTER
#if USE_GUTTER == GUTTER_QUARTER
            } else if ((p - addr) % QUARTER_WIDTH == 0) {
#else
            } else if ((p - addr) % HALF_WIDTH == 0) {
#endif // USE_GUTTER == GUTTER_QUARTER
                // Divide WIDTH byte hex output into two HALF_WIDTH byte sections for legibility
                strcat(line_buf, RESET " ");
#endif // USE_GUTTER
            }
        }

        // Display hex output in two byte clumps
#if USE_COLOUR
        strcat(line_buf, COLOUR(odd_row, (p-addr) % WIDTH));
#endif

        // Append hex representation
        snprintf(hex_buf, 3, "%02x", *(unsigned char*)p);
        strcat(line_buf, hex_buf);
#if USE_HEX_SEPARATOR
#if USE_HEX_SEPARATOR == HEX_PAIR_SEPARATOR
        if((p - addr) % USE_HEX_SEPARATOR) {
#endif
            if (((p + 1 - addr) % WIDTH) != 0) {
                strcat(line_buf, RESET " ");
            }
#if USE_HEX_SEPARATOR == HEX_PAIR_SEPARATOR
        }
#endif
#endif
    }

    // There may be some char representation lef to handle at this point.
    if (line_src) {
        // Add any padding spaces
        if (remaining < WIDTH) {
            int pads = 0;

#if USE_GUTTER == GUTTER_QUARTER
            if (remaining <= QUARTER_WIDTH) {
                 pads += 3;
            } else if (remaining <= HALF_WIDTH) {
                 pads += 2;
            } else if (remaining <= THREE_QUARTER_WIDTH) {
                 pads += 1;
            }
#elif USE_GUTTER == GUTTER_HALF
            if (remaining <= HALF_WIDTH) {
                 pads += 1;
            }
#endif // USE_GUTTER

            pads += (WIDTH - remaining) * 2; // Two hex chars for each byte

#if USE_HEX_SEPARATOR == HEX_PAIR_SEPARATOR
            // One separator for each pair of bytes bar the last one
            pads += (((WIDTH - remaining) + 1) / 2) - 1;
#elif USE_HEX_SEPARATOR == HEX_UNIT_SEPARATOR
            // One separator for each byte bar the last one
            pads += (WIDTH - remaining) - 1;
#endif // USE_HEX_SEPARATOR


#if USE_COLOUR
            strcat(line_buf, RESET);
#endif // USE_COLOUR

            int i = strlen(line_buf);
            memset((void*)&line_buf[i], ' ', pads);
            line_buf[i + pads] = '\0';
        }

        _memdump_append_char_display(odd_row, line_buf, line_src, remaining);
        fprintf(file, LINE_FMT, line_src, line_buf);
    }

    fprintf(file, "\n");
}

// -----------------------------------------------------------------------------
