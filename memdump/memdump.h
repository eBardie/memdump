#ifndef MEMDUMP_H
#define MEMDUMP_H

#include <stdio.h>

void fmemdump(FILE* file, char* desc, void *addr, unsigned int lenin);

inline void memdump(char* desc, void *addr, unsigned int lenin){ fmemdump(stdout, desc, addr, lenin); };
inline void ememdump(char* desc, void *addr, unsigned int lenin){ fmemdump(stderr, desc, addr, lenin); };

// Implement a variable called `memdump_colourscheme`, of type
// `memdump_colourscheme_t`, in your own code to override any default
// colourscheme.
//
// Fill in the two dimension array with values from `memdump_colours.h`.
//
// e.g.
//
//     #include "memdump_colours.h"
//
//     memdump_colourscheme_t memdump_colourscheme_t = {
//         {
//             HYEL, HYEL, HYEL, HYEL,
//             HYEL, HYEL, HYEL, HYEL,
//             HYEL, HYEL, HYEL, HYEL,
//             HYEL, HYEL, HYEL, HYEL,
//         },{
//             YEL, YEL, YEL, YEL,
//             YEL, YEL, YEL, YEL,
//             YEL, YEL, YEL, YEL,
//             YEL, YEL, YEL, YEL,
//         }
//     };
//
// See `memdump.c` for more examples.
//
// Note a default colourscheme must exist for this to work.

typedef char* memdump_colourscheme_t[2][16];

#endif
