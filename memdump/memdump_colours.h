#ifndef _MEMDUMP_COLOURS_H
#define _MEMDUMP_COLOURS_H

#define  BLK    "\e[30m"
#define  RED    "\e[31m"
#define  GRN    "\e[32m"
#define  YEL    "\e[33m"
#define  BLU    "\e[34m"
#define  MAG    "\e[35m"
#define  CYN    "\e[36m"
#define  WHT    "\e[37m"
#define HBLK    "\e[90m"
#define HRED    "\e[91m"
#define HGRN    "\e[92m"
#define HYEL    "\e[93m"
#define HBLU    "\e[94m"
#define HMAG    "\e[95m"
#define HCYN    "\e[96m"
#define HWHT    "\e[97m"
// Bold
#define  BOL    "\e[1m"
#define UNBOL   "\e[22m"
#define  BLKB   "\e[1;30m"
#define  REDB   "\e[1;31m"
#define  GRNB   "\e[1;32m"
#define  YELB   "\e[1;33m"
#define  BLUB   "\e[1;34m"
#define  MAGB   "\e[1;35m"
#define  CYNB   "\e[1;36m"
#define  WHTB   "\e[1;37m"
#define HBLKB   "\e[1;90m"
#define HREDB   "\e[1;91m"
#define HGRNB   "\e[1;92m"
#define HYELB   "\e[1;93m"
#define HBLUB   "\e[1;94m"
#define HMAGB   "\e[1;95m"
#define HCYNB   "\e[1;96m"
#define HWHTB   "\e[1;97m"
// Faint
#define FAINT   "\e[2m"
#define UNFAINT "\e[22m"
#define  BLKF   "\e[2;30m"
#define  REDF   "\e[2;31m"
#define  GRNF   "\e[2;32m"
#define  YELF   "\e[2;33m"
#define  BLUF   "\e[2;34m"
#define  MAGF   "\e[2;35m"
#define  CYNF   "\e[2;36m"
#define  WHTF   "\e[2;37m"
#define HBLKF   "\e[2;90m"
#define HREDF   "\e[2;91m"
#define HGRNF   "\e[2;92m"
#define HYELF   "\e[2;93m"
#define HBLUF   "\e[2;94m"
#define HMAGF   "\e[2;95m"
#define HCYNF   "\e[2;96m"
#define HWHTF   "\e[2;97m"
// Underline
#define  UND    "\e[4m"
#define  BLKU   "\e[4;30m"
#define  REDU   "\e[4;31m"
#define  GRNU   "\e[4;32m"
#define  YELU   "\e[4;33m"
#define  BLUU   "\e[4;34m"
#define  MAGU   "\e[4;35m"
#define  CYNU   "\e[4;36m"
#define  WHTU   "\e[4;37m"
#define HBLKU   "\e[4;90m"
#define HREDU   "\e[4;91m"
#define HGRNU   "\e[4;92m"
#define HYELU   "\e[4;93m"
#define HBLUU   "\e[4;94m"
#define HMAGU   "\e[4;95m"
#define HCYNU   "\e[4;96m"
#define HWHTU   "\e[4;97m"
// Background colour
#define  BLKBG  "\e[97;40m"  // White FG
#define  REDBG  "\e[97;41m"  //   "   FG
#define  GRNBG  "\e[97;42m"  //   "   FG
#define  YELBG  "\e[97;43m"  //   "   FG
#define  BLUBG  "\e[97;44m"  //   "   FG
#define  MAGBG  "\e[97;45m"  //   "   FG
#define  CYNBG  "\e[97;46m"  //   "   FG
#define  WHTBG  "\e[30;47m"  // Black FG
#define HBLKBG  "\e[97;100m" // White FG
#define HREDBG  "\e[97;101m" //   "   FG
#define HGRNBG  "\e[97;102m" //   "   FG
#define HYELBG  "\e[97;103m" //   "   FG
#define HBLUBG  "\e[97;104m" //   "   FG
#define HMAGBG  "\e[97;105m" //   "   FG
#define HCYNBG  "\e[97;106m" //   "   FG
#define HWHTBG  "\e[30;107m" // Black FG

#define RESET   "\e[0m"
#define MAX_COLOUR_CODE_LEN 13  // Note that this value is hand calculated and
                                // if you add longer codes will require
                                // revising

#endif // _MEMDUMP_COLOURS_H
