SRCS := memdump/memdump.c memdump_test.c
HDRS := memdump/memdump.h memdump/memdump_colours.h
TESTBIN := memdump_test

CFLAGS += -Imemdump

$(TESTBIN): $(SRCS) $(HDRS)
	$(CC) $(CFLAGS) -o $@ $(SRCS)

test: $(TESTBIN)
	./$(TESTBIN) Makefile /dev/null /proc/cmdline

test_override:
	@make CFLAGS="-DMEMDUMP_TEST_COLOURSCHEME_OVERRIDE=1 $(CFLAGS)" clean test
	@make clean

clean:
	@rm $(TESTBIN) || true
